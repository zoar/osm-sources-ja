# Source tags, Used in the Japan area data from OpenStreetMap

Which way I have created.

OpenStreetMap data of July 1 2015  
Download from [OpenStreetMap Data Extracts](http://download.geofabrik.de/)

```
$ wget http://download.geofabrik.de/asia/japan-latest.osm.bz2
$ bunzip2 japan-latest.osm.bz2
```

Pulled out the Source tag from OSM File.

```
$ grep "<tag k=\"source\" v=" ./japan-latest.osm |\
 sed s/"\t\t.*v=\""/""/ |\
 sed s/"\"\/>"/""/ > sources.txt
```

Exclude duplicate.

```
$ cat sources.txt | sort | uniq > source_uniq.txt
```

Copylight (c) [OpenStreetMap contributors](http://www.openstreetmap.org/copyright)  
[Released Under the Creative Commons Attribution-ShareAlike 2.0](http://creativecommons.org/licenses/by-sa/2.0/)  
